import random
from os import listdir
from os.path import isfile, join
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import telegram
import yaml
from types import SimpleNamespace
import random

class Humility:
	def __init__(self, config):
		self.config = SimpleNamespace(**config)
		self.updater = Updater(self.config.token)
		self.dispatcher = self.updater.dispatcher
		self.dispatcher.add_handler(CommandHandler('pedique', self.pedique))
		self.dispatcher.add_handler(CommandHandler('norma', self.set_normie))
		self.dispatcher.add_handler(MessageHandler(Filters.text, self.text))
		# self.dispatcher.add_handler(MessageHandler(Filters.sticker, self.test))

		with open('normies.txt') as no:
			self.normies = no.readlines()

		with open('norma.txt') as n:
			self.norma = n.readlines()

	def text(self, bot, update):
		print("ID: %d, : username: %s, message: %s" % (update.message.from_user.id,update.message.from_user.username, update.message.text))
		if update.message.from_user.id in self.config.victims and update.message.message_id % 10 == 1:
			path = './'+str(update.message.from_user.id)
			try:
				pictures = [path+'/'+f for f in listdir(path) if isfile(join('./'+path, f))]
				update.message.reply_photo(photo=open(random.choice(pictures), 'rb'))
			except Exception as e:
				print(e)

		if update.message.from_user.id == self.config.shad_id:
			if update.message.message_id % 3 == 1:
				self.test(bot, update)

		print(self.normies)
		if str(update.message.from_user.id) in str(self.normies):
			print('normie catched!')
			try:
				update.message.reply_text(self.martin_alekseevich(update.message.text))
			except Exception as e:
				print(e)

	def pedique(self, bot, update):
		print('Sending pedique...')
		try:
			pictures = ['./'+str(self.config.pedique_id)+"/"+f for f in listdir('./'+str(self.config.pedique_id)) if isfile(join('./'+str(self.config.pedique_id), f))]
			update.message.reply_photo(photo=open(random.choice(pictures), 'rb'))
		except Exception as e:
			print(e)

	def test(self, bot, update):
		print(update.message)
		with open('vasenko.txt', 'r') as v:
			stickers = v.readlines()
			sticker = random.choice(stickers).rstrip()
			try:
				update.message.reply_sticker(sticker=sticker)
			except Exception as e:
				print(e)

	def set_normie(self, bot, update):
		if update.message.from_user.id not in self.config.admins:
			return

		print('set normie')
		normie_id = update.message.reply_to_message.from_user.id
		normie_name = update.message.reply_to_message.from_user.name
		self.normies.append(normie_id)
		with open('normies.txt', 'a') as n:
			n.write(normie_id)
		message.reply_text(text='Здравствуйте, уважаемый %s!' % (name,),
                   parse_mode=telegram.ParseMode.MARKDOWN)

	def martin_alekseevich(self, text):
		try:
			norma = self.norma[random.randint(0, len(self.norma)-1)]
			words = norma.split(' ')
			shits = text.split(' ')
			i = len(words)-1
			j = len(shits)-1

			while(i > 0 and j > 0):
				words[random.randint(0, len(words)-1)] = shits[random.randint(0, len(shits)-1)]
				i -= 1
				j -= 1

			return " ".join(words)
		except Exception as e:
			print(e)

	def start_polling(self):
		self.updater.start_polling()

def main(config):
    humility = Humility(config)
    humility.start_polling()

if __name__ == '__main__':
	with open('config.yml', 'r') as f:
		config = yaml.load(f)
	main(config)
